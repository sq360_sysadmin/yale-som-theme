#Yale School of Management Theme

##Installation

```
npm install -S git+https://git@bitbucket.org:sq360_sysadmin/yale-som-theme.git
```

##Importing

###Main Theme Files

```scss
@import "~yale-som-theme/scss/yale-som-theme";
```

It also include the `Yale` and `NeueHaasUnicaPro` and font families.

###Additional Fonts Families

```scss
@import "~yale-som-theme/scss/fonts/_font-arrow";
```

```scss
@import "~yale-som-theme/scss/fonts/fontello/_fontello";
```

```scss
@import "~yale-som-theme/scss/fonts/linearicons/_linearicons";
```

```scss
@import "~yale-som-theme/scss/fonts/ss-social/_ss-social-circle";
```

```scss
@import "~yale-som-theme/scss/fonts/ss-social/_ss-social-regular";
```

```scss
@import "~yale-som-theme/scss/fonts/ss-standard/_ss-standard-custom";
```

```scss
@import "~yale-som-theme/scss/fonts/ss-standard/_ss-standard";
```

```scss
@import "~yale-som-theme/scss/fonts/_ss-symbolicons-block";
```